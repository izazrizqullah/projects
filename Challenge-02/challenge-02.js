const readline = require("readline");
const r = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function n(question) {
  return new Promise((resolve) => {
    r.question(question, (data) => {
      return resolve(data);
    });
  });
}

let nilai = [];

function tampungNilai(na) {
  let nilaiMhs = nilai.push(na);
  return nilaiMhs;
}

function minAndMax() {
  const hasilMin = Math.min.apply(null, nilai);
  const hasilMax = Math.max.apply(null, nilai);
  const hasil = `
  Nilai min : ${hasilMin}
  Nilai max : ${hasilMax}`;
  return hasil;
}

function valAverage() {
  let hasilAvg = nilai.reduce((a, b) => a + b, 0) / nilai.length;
  const hasil = `Rata-rata nilai siswa adalah ${hasilAvg}`;
  return hasil;
}

function jmlLulus() {
  let kritLulus = nilai.filter((x) => (x) => 60).length;
  let kritTLulus = nilai.filter((y) => y < 60).length;
  const hasil = `
  Siswa Lulus : ${kritLulus}
  Siswa Tidak Lulus : ${kritTLulus}
  `;
  return hasil;
}

function findValue() {
  let filter1 = nilai.filter((x) => x == 90);
  let filter2 = nilai.filter((x) => x == 100);
  const hasil = `
  Nilai 90 & 100 : ${filter1},${filter2}
  `;
  return hasil;
}

function sorting() {
  let sortNilai = nilai.sort((a, b) => a - b);
  let hasil = `Min to Max : ${sortNilai}`;
  return hasil;
}

async function main() {
  let looping = true;
  while (looping) {
    try {
      let inputNilai = await n("Masukan nilai Mhs : ");
      let hasilInput = tampungNilai(+inputNilai);
      let inputLagi = await n("Apakah ingin memasukan nilai lagi?[y/n] : ");
      if (inputLagi === "y".toLowerCase()) {
      } else if (inputLagi === "n") {
        looping = false;
        looping1 = true;
        console.log(`Pilih menu :
        1. Mencari nilai min & max
        2. Mencari rata rata nilai
        3. Jumlah siswa lulus
        4. Urutkan nilai dari min to max
        5. Cari siswa nilai 90 & 100`);
        while (looping1) {
          let menu = await n("Masukan menu : ");
          switch (menu) {
            case "1":
              console.log("Mencari nilai min dan max");
              let hasil1 = minAndMax();
              console.log(hasil1);
              break;
            case "2":
              console.log("Mencari rata rata nilai");
              let hasilRerata = valAverage();
              console.log(hasilRerata);
              break;
            case "3":
              console.log("Jumlah siswa lulus dan tidak lulus ");
              let cekJml = jmlLulus();
              console.log(cekJml);
              break;
            case "4":
              console.log("Urutkan nilai dari min to max");
              let sortingNilai = sorting();
              console.log(sortingNilai);
              break;
            case "5":
              console.log("Mencari siswa dengan nilai 90 & 100");
              let find = findValue();
              console.log(find);
              break;
            default:
              console.log("Pilihan salah!");
              console.log("Terima kasih sudah menggunakan program!");
              looping1 = false;
              r.close();
              break;
          }
        }
      } else {
        console.log("Pilihan salah!");
        console.log("Anda telah membatalkan program!");
        looping = false;
        r.close();
      }
    } catch (err) {
      console.log(err);
    }
  }
}

main();
