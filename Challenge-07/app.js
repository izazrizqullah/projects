require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const router = require("./routes");
const cookieParser = require("cookie-parser");
const app = express();
const { HTTP_PORT } = process.env;

app.use(express.json());
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.set("view engine", "ejs");

app.use("/api", router);

// handling 500
app.use((req, res, next) => {
  return res.status(400).json({
    status: false,
    message: "Are you lost?",
  });
});

// handling 500
app.use((req, res, next) => {
  return res.status(500).json({
    status: false,
    message: err.message,
  });
});

app.listen(HTTP_PORT, () => {
  console.log(`listening on port ${HTTP_PORT}`);
});
