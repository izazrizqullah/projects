// Nama : Izaz Rizqullah
// Kelas : BEJS-1

const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function input(question) {
  return new Promise((resolve) => {
    rl.question(question, (data) => {
      return resolve(data);
    });
  });
}

const tambah = (angka1, angka2) => {
  let hasil = +angka1 + +angka2;
  return hasil;
};

const kurang = (angka1, angka2) => {
  let hasil = +angka1 - +angka2;
  return hasil;
};

const bagi = (angka1, angka2) => {
  let hasil = +angka1 / +angka2;
  return hasil;
};

const kali = (angka1, angka2) => {
  let hasil = +angka1 * +angka2;
  return hasil;
};

const akarKuadrat = (angka) => Math.sqrt(angka);

const luasPersegi = (sisi) => sisi ** 2;

const volumeKubus = (sisi) => sisi ** 3;

const volumeTabung = (r, t) => 3.14 * r ** 2 * t;

async function main() {
  try {
    let aplikasi = true;
    while (aplikasi == true) {
      console.log("Kalkulator Sederhana");
      console.log(`Menu : 
    1. Pertambahan
    2. Pembagian
    3. Pengurangan
    4. Perkalian
    5. Akar Kuadrat
    6. Luas Persegi
    7. Volume Kubus
    8. Volume Tabung
    9. Keluar`);
      let pilihan = await input("Masukan pilihan 1 - 9 : ");

      if (pilihan === "1") {
        console.log("Anda memilih pertambahan!");
        let input1 = await input("Masukan input 1 : ");
        let input2 = await input("Masukan input 2 : ");
        let hasil = tambah(input1, input2);

        console.log("===========================================");
        console.log(`Hasil dari ${input1} + ${input2} =`, hasil);
        console.log("===========================================");
      } else if (pilihan === "2") {
        console.log("Anda memilih pembagian!");
        let input1 = await input("Masukan input 1 : ");
        let input2 = await input("Masukan input 2 : ");
        let hasil = bagi(input1, input2);

        console.log("===========================================");
        console.log(`Hasil dari ${input1} / ${input2} =`, hasil);
        console.log("===========================================");
      } else if (pilihan === "3") {
        console.log("Anda memilih pengurangan!");
        let input1 = await input("Masukan input 1 : ");
        let input2 = await input("Masukan input 2 : ");
        let hasil = kurang(input1, input2);

        console.log("===========================================");
        console.log(`Hasil dari ${input1} - ${input2} =`, hasil);
        console.log("===========================================");
      } else if (pilihan === "4") {
        console.log("Anda memilih perkalian!");
        let input1 = await input("Masukan input 1 : ");
        let input2 = await input("Masukan input 2 : ");
        let hasil = kali(input1, input2);

        console.log("===========================================");
        console.log(`Hasil dari ${input1} * ${input2} =`, hasil);
        console.log("===========================================");
      } else if (pilihan === "5") {
        console.log("Anda memilih akar kuadrat!");
        let input1 = await input("Masukan input : ");
        let hasil = akarKuadrat(input1);

        console.log("===========================================");
        console.log(`Hasil akar kuadrat dari ${input1} =`, hasil);
        console.log("===========================================");
      } else if (pilihan === "6") {
        console.log("Anda memilih luas persegi!");
        let sisi = await input("Masukan sisi : ");
        let hasil = luasPersegi(sisi);

        console.log("===========================================");
        console.log(`Hasil luas persegi dari ${sisi} * ${sisi} =`, hasil);
        console.log("===========================================");
      } else if (pilihan === "7") {
        console.log("Anda memilih volume kubus!");
        let sisi = await input("Masukan sisi : ");
        let hasil = volumeKubus(sisi);

        console.log("===========================================");
        console.log(
          `Hasil volume kubus dari ${sisi} * ${sisi} * ${sisi} =`,
          hasil
        );
        console.log("===========================================");
      } else if (pilihan === "8") {
        console.log("Anda memilih volume tabung");
        const phi = 3.14;
        let r = await input("Masukan jari-jari : ");
        let t = await input("Masukan tinggi : ");
        let hasil = volumeTabung(r, t);

        console.log("===========================================");
        console.log(
          `Hasil volume tabung dari 3.14 * ${r} * ${r} * ${t} =`,
          hasil
        );
        console.log("===========================================");
      } else if (pilihan === "9") {
        console.log("===========================================");
        console.log("Terimakasih sudah menggunakan aplikasi ini!");
        aplikasi = false;
        console.log("===========================================");
        rl.close();
        break;
      } else {
        console.log("===========================================");
        console.log("Pilihan salah!");
        console.log("===========================================");
      }

      let tanya = await input("Apakah ingin membuka aplikasi lagi [y/t] : ");
      let tanya1 = tanya.toLowerCase();
      if (tanya1 == "y") {
        console.log("Selamat menggunakan aplikasi lagi!");
        console.clear();
      } else if (tanya1 == "t") {
        console.log("===========================================");
        console.log("Terimakasih sudah menggunakan aplikasi ini!");
        console.log("===========================================");
        aplikasi = false;
        rl.close();
      } else {
        console.log("===========================================");
        console.log("Pilihan salah! Anda membatalkan transaksi");
        console.log("===========================================");
        aplikasi = false;
        rl.close();
      }
    }
  } catch (err) {
    console.log(err);
  }
}

main();
