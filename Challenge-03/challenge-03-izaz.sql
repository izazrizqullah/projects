-- CREATE TABLE TBEMPLOYEES
CREATE TABLE tbemployees (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    address TEXT NOT NULL,
    position_id INTEGER NOT NULL
);

-- CREATE TABLE TBPOSITIONS
CREATE TABLE tbpositions (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

-- CREATE TABLE TBATTENDANCES
CREATE TABLE tbattendances (
    id BIGSERIAL PRIMARY KEY,
    employee_id INTEGER NOT NULL,
    datee VARCHAR(255) NOT NULL,
    sign_in VARCHAR(255) NOT NULL,
    sign_out VARCHAR(255) NOT NULL
);

-- CREATE TABLE TBABSENCES
CREATE TABLE tbabsences (
    id BIGSERIAL PRIMARY KEY,
    employee_id INTEGER NOT NULL,
    datee VARCHAR(255) NOT NULL,
    permission VARCHAR(255) NOT NULL,
    description TEXT NOT NULL DEFAULT '-'
);

-- CREATE TABLE TBADMIN
CREATE TABLE tbadmin (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    employee_id INTEGER NOT NULL
);

-- CREATE DATA
-- INSERT DATA TBEMPLOYEES
INSERT INTO tbemployees (name, email, address, position_id) VALUES
('farrel','farrel13@gmail.com','jakarta',1),
('izaz','izaz@gmail.com','warsaw',2),
('aleksandra','szepanska@gmail.com','krakow',3),
('setya','setya@gmail.com','surabaya',4),
('zain','zain@gmail.com','banjarnegara',1);

-- INSERT DATA TBPOSITIONS
INSERT INTO tbpositions (name) VALUES
('Mobile Developer'),
('Backend Developer'),
('Copywriter'),
('Fullstack Developer');

-- INSERT DATA TBATENDANCES
INSERT INTO tbattendances (employee_id,datee,sign_in,sign_out) VALUES
(1,'2022-09-09','07.50','16.30'),
(2,'2022-09-09','07.52','16.28'),
(3,'2022-09-09','07.48','16.35'),
(4,'2022-09-09','07.55','16.35'),
(5,'2022-09-09','08.00','17.00');

-- INSERT DATA TBABSENCES
INSERT INTO tbabsences (employee_id,datee,permission,description) VALUES
('1','2022-09-10','sick','cold'),
('4','2022-09-11','alpha','no reasons');

-- INSERT DATA TBADMIN
INSERT INTO tbadmin (username, password, employee_id) VALUES
('admin1','admin123',1),
('admin2','admin123',2);



-- READ DATA
-- SELECT DATA TBEMPLOYEES
SELECT * FROM tbemployees;
SELECT * FROM tbemployees WHERE id=2;
SELECT tbemployees.*, tbpositions.name
FROM tbemployees JOIN tbpositions ON tbpositions.id = tbemployees.position_id;

-- SELECT DATA TBPOSITIONS
SELECT * FROM tbpositions;
SELECT * FROM tbpositions where name = 'Copywriter';
SELECT
tbemployees.name AS name,
tbpositions.name AS position
FROM tbpositions JOIN tbemployees
ON tbemployees.position_id = tbpositions.id;

-- SELECT DATA TBATTENDANCES
SELECT * FROM tbattendances;
SELECT
tbemployees.id AS employee_id,
tbemployees.name AS name,
tbattendances.datee AS date,
tbattendances.sign_in AS sign_in,
tbattendances.sign_out AS sign_out
FROM tbattendances
JOIN tbemployees ON tbemployees.id = tbattendances.employee_id;

-- SELECT DATA TBABSENCE
SELECT * FROM tbabsences;
SELECT
tbemployees.id AS employee_id,
tbemployees.name AS name,
tbabsences.datee AS date,
tbabsences.permission AS permission,
tbabsences.description AS description
FROM tbabsences
JOIN tbemployees ON tbemployees.id = tbabsences.employee_id;

-- SELECT DATA TBADMIN;
SELECT * FROM tbadmin where username = 'admin1';
SELECT
tbemployees.id AS employee_id,
tbemployees.name AS name,
tbadmin.username AS username,
tbadmin.password AS password
FROM tbadmin
RIGHT JOIN tbemployees ON tbemployees.id = tbadmin.employee_id;



-- UPDATE
-- UPDATE DATA TBEMPLOYEES
UPDATE tbemployees SET address = 'purwokerto' WHERE id = 1;

-- UPDATE DATA TBPOSITIONS
UPDATE tbpositions SET name = 'Product Management' WHERE id = 3;

-- UPDATE DATA TBATTENDANCES
UPDATE tbattendances SET datee = '2022-09-10' WHERE employee_id = 5;

-- UPDATE DATA TBABSENCES
UPDATE tbabsences SET permission = 'alpha' WHERE id = 1;

-- UPDATE DATA TBADMIN
UPDATE tbadmin SET password = '123admin' WHERE id = 1;



-- DELETE
-- DELETE DATA TBEMPLOYEES
DELETE FROM tbemployees WHERE address = 'jakarta';

-- DELETE DATA TBPOSITIONS
DELETE FROM tbpositions WHERE id = 3;

-- DELETE DATA TBATTENDANCES
DELETE FROM tbattendances WHERE sign_out = '17.00';

-- DELETE DATA TBABSENCES
DELETE FROM tbabsences WHERE datee = '2022-09-10';

-- DELETE DATA TBADMIN
DELETE FROM tbadmin WHERE username = 'admin2';