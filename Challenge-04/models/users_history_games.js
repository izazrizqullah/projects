'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class users_history_games extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  users_history_games.init({
    game: DataTypes.STRING,
    time: DataTypes.STRING,
    score: DataTypes.STRING,
    win: DataTypes.STRING,
    lose: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'users_history_games',
  });
  return users_history_games;
};