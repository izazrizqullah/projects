'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class users_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  users_game_history.init({
    game: DataTypes.STRING,
    time: DataTypes.STRING,
    score: DataTypes.STRING,
    win: DataTypes.STRING,
    lose: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'users_game_history',
  });
  return users_game_history;
};