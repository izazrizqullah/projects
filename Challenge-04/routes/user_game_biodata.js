const express = require("express");
const router = express.Router();
const m = require("../helper");
const controller = require("../controllers");

router.get("/", m.mustLogin, controller.users_game_biodata.getBioAll);
router.get("/:id", controller.users_game_biodata.getBio);
router.post("/", controller.users_game_biodata.createBio);
router.put("/:id", controller.users_game_biodata.updateBio);
router.delete("/:id", controller.users_game_biodata.deleteBio);

module.exports = router;
