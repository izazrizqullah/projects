const express = require("express");
const router = express.Router();
const controller = require("../controllers");

router.get("/", controller.users_game.getUsers);
router.get("/:id", controller.users_game.getDetailsUsers);
router.post("/", controller.users_game.createUser);
router.put("/:id", controller.users_game.updateUser);
router.delete("/:id", controller.users_game.deleteUser);
router.post("/login", controller.users_game.login);

module.exports = router;
