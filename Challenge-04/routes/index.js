const express = require("express");
const user_game = require("./user_game");
const user_game_biodata = require("./user_game_biodata");
const users_history_game = require("./users_history_game");
const mid = require("../helper/middleware");
const router = express.Router();

router.use("/users", user_game);
router.use("/bio", mid.mustLogin, user_game_biodata);
router.use("/history", users_history_game);

module.exports = router;
