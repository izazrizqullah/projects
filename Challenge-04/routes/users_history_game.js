const express = require("express");
const router = express.Router();
const controller = require("../controllers");

router.get("/", controller.users_history_game.getAllHistory);
router.get("/:id", controller.users_history_game.getHistory);
router.post("/", controller.users_history_game.createHistory);
router.put("/:id", controller.users_history_game.updateHistory);
router.delete("/:id", controller.users_history_game.deleteHistory);

module.exports = router;
