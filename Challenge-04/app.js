require("dotenv").config();

const express = require("express");
const app = express();
const morgan = require("morgan");
const controller = require("./controllers");
const { HTTP_PORT } = process.env;
const routes = require("./routes");

app.use(express.json());
app.use(morgan("dev"));
app.set("view engine", "ejs");
app.use(routes);

app.get("/", (_req, res) => {
  res.render("home");
});

// 404 Handler
app.use(controller.notFound);

// // 500 Handler
app.use(controller.exception);

app.listen(HTTP_PORT, () => {
  console.log(`listening on port ${HTTP_PORT}`);
});
