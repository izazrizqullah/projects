const { users_game } = require("../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const { JWT_SIGNATURE_KEY } = process.env;

module.exports = {
  getUsers: async (req, res) => {
    try {
      const allUser = await users_game.findAll();

      if (allUser.length <= 0) {
        res.status(404).json({
          status: "failed",
          message: "data was not-found",
          data: null,
        });
      }

      return res.status(200).json({
        status: "success",
        message: "get data successful!",
        data: allUser,
      });
    } catch (err) {
      next(err);
    }
  },
  getDetailsUsers: async (req, res, next) => {
    try {
      const { id } = req.params;
      const findUser = await users_game.findOne({
        where: {
          id: id,
        },
      });

      if (!findUser) {
        return res.status(404).json({
          status: "failed",
          message: "data was not-found",
          data: null,
        });
      }

      return res.status(200).json({
        status: "success",
        message: "get data successful!",
        data: findUser,
      });
    } catch (err) {
      next(err);
    }
  },
  createUser: async (req, res, next) => {
    try {
      const { name, username, email, password } = req.body;

      const existUser = await users_game.findOne({
        where: {
          email: email,
        },
      });

      if (existUser) {
        return res.status(409).json({
          status: "failed",
          message: "email already used!",
          data: null,
        });
      }

      const encryptedPassword = await bcrypt.hash(password, 10);

      const user = await users_game.create({
        name,
        username,
        email,
        password: encryptedPassword,
      });

      return res.status(201).json({
        status: "success",
        message: "create data successful!",
        data: {
          name: users_game.name,
          username: users_game.username,
          email: users_game.email,
          password: users_game.password,
        },
      });
    } catch (err) {
      next(err);
    }
  },
  updateUser: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { name, username, email, password } = req.body;

      if (name && username && email && password) {
        const user = await users_game.findAll({
          where: {
            id: id,
          },
        });
        const updateData = await users_game.update(
          {
            name,
            email,
            username,
            password,
          },
          {
            where: {
              id,
            },
          }
        );

        res.status(200).json({
          status: "success",
          message: "update data successfull",
          data: updateData,
        });
      } else {
        return res.status(404).json({
          status: "failed",
          message: "data was not found",
          data: null,
        });
      }
    } catch (err) {
      next(err);
    }
  },
  deleteUser: async (req, res, next) => {
    try {
      const { id } = req.params;

      const user = await users_game.findOne({
        where: {
          id: id,
        },
      });

      const deleted = await users_game.destroy({
        where: {
          id: id,
        },
      });

      if (user.length <= 0) {
        return res.status(404).json({
          status: "failed",
          message: "delete data failed",
          data: null,
        });
      }

      return res.status(200).json({
        status: "success",
        message: "delete data successful!",
        data: user,
      });
    } catch (err) {
      next(err);
    }
  },
  login: async (req, res, next) => {
    try {
      const { username, password } = req.body;

      const user = await users_game.findAll({
        where: {
          username: username,
        },
      });

      if (!user) {
        return res.status(400).json({
          status: "failed",
          message: "username or password doesn't match",
        });
      }

      const correct = await bcrypt.compare(
        password,
        users_game.password,
        (err, res) => {
          if (err) {
            console.log(err);
          }
        }
      );

      payload = {
        id: users_game.id,
        username: users_game.username,
        password: users_game.password,
      };

      const token = jwt.sign(payload, JWT_SIGNATURE_KEY);

      return res.status(200).json({
        status: "success",
        message: "login successful!",
        data: {
          token: token,
        },
      });
    } catch (err) {
      next(err);
    }
  },
  changePassword: async (req, res, next) => {
    try {
    } catch (err) {
      next(err);
    }
  },
};
