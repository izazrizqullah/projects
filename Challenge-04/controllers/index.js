const users_game = require("./user_game");
const users_game_biodata = require("./user_game_biodata");
const users_history_game = require("./users_history_game");

module.exports = {
  exception: (err, req, res, next) => {
    res.render("server-error", { error: err.message });
  },
  notFound: (_req, res, _next) => {
    res.render("not-found");
  },
  users_game,
  users_game_biodata,
  users_history_game,
};
