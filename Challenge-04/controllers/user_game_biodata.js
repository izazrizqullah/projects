const { users_biodata } = require("../models");

module.exports = {
  getBio: async (req, res) => {
    try {
      const { id } = req.params;

      const users = await users_biodata.findAll({
        where: {
          id: id,
        },
      });

      if (!user) {
        return res.status(404).json({
          status: "failed",
          message: "get data failed",
          data: null,
        });
      }

      return res.status(200).json({
        status: "success",
        message: "get data successful",
        data: users,
      });
    } catch (err) {
      next(err);
    }
  },
  getBioAll: async (req, res, next) => {
    try {
      const { id } = req.params;

      const bio = await users_biodata.findAll();

      if (!bio) {
        return res.status(404).json({
          status: "failed",
          message: "data was not found",
          data: null,
        });
      }

      return res.status(200).json({
        status: "success",
        message: "get data successful!",
        data: bio,
      });
    } catch (errr) {
      next(err);
    }
  },
  createBio: async (req, res, next) => {
    try {
      const { name, age, email, address } = req.body;

      // const user = req.user;

      // if (!user.username) {
      //   return res.status(404).json({
      //     status: "failed",
      //     message: "not found",
      //     data: null,
      //   });
      // }

      const bio = await users_biodata.create({
        name,
        age,
        email,
        address,
      });

      return res.status(201).json({
        status: "success",
        message: "create data successful!",
        data: {
          bio,
        },
      });
    } catch (err) {
      console.log(err);
    }
  },
  updateBio: async (req, res, next) => {
    try {
      const { id } = req.params;

      const { name, age, email, address } = req.body;

      const findBio = await users_biodata.findOne({
        where: {
          id: id,
        },
      });

      if (!findBio) {
        return res.status(404).json({
          status: "failed",
          message: "data was not found",
          data: null,
        });
      }

      const updateBio = await users_biodata.update(
        {
          name,
          age,
          email,
          address,
        },
        {
          where: {
            id: id,
          },
        }
      );

      return res.status(200).json({
        status: "success",
        message: "update data successful!",
        data: findBio,
      });
    } catch (err) {
      next(err);
    }
  },
  deleteBio: async (req, res, next) => {
    try {
      const { id } = req.params;

      const findBio = users_biodata.findOne({
        where: {
          id: id,
        },
      });

      const deleted = users_biodata.destroy({
        where: {
          id: id,
        },
      });

      if (findBio.length <= 0) {
        return res.status(404).json({
          status: "failed",
          message: "data was not found",
          data: null,
        });
      }

      return res.status(200).json({
        status: "success",
        message: "delete data successful",
      });
    } catch (err) {
      next(err);
    }
  },
};
