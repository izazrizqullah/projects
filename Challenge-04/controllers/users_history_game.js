const { users_history_games } = require("../models");

module.exports = {
  getHistory: async (req, res, next) => {
    const { id } = req.params;

    const findHistory = await users_history_games.findOne({
      where: {
        id: id,
      },
    });

    if (!findHistory) {
      return res.status(404).json({
        status: "failed",
        message: "data was not found",
        data: null,
      });
    }

    return res.status(200).json({
      status: "success",
      message: "get data successful!",
      data: findHistory,
    });
  },
  getAllHistory: async (req, res, next) => {
    const getAll = await users_history_games.findAll();

    if (getAll.length <= 0) {
      return res.status(404).json({
        status: "failed",
        message: "data was not found",
        data: null,
      });
    }

    return res.status(200).json({
      status: "success",
      message: "get data successful!",
      data: getAll,
    });
  },
  createHistory: async (req, res, next) => {
    try {
      const { game, time, score, win, lose } = await req.body;
      // const game = "mobile legend";
      // const time = "mobile legend";
      // const score = "mobile legend";
      // const win = "mobile legend";
      // const lose = "mobile legend";

      const created = await users_history_games.create({
        game,
        time,
        score,
        win,
        lose,
      });

      console.log(game); //undefined
      return res.status(201).json({
        status: "success",
        message: "create data successful!",
        data: created,
      });
    } catch (err) {
      next(err);
    }
  },
  updateHistory: async (req, res, next) => {
    const { id } = req.params;

    const { game, time, score, win, lose } = req.body;

    if (game && time && score && win && lose) {
      const updated = users_history_game.update(
        {
          game,
          time,
          score,
          win,
          lose,
        },
        {
          where: {
            id: id,
          },
        }
      );

      return res.status(200).json({
        status: "success",
        message: "update data successful!",
        data: updated,
      });
    }
  },
  deleteHistory: async (req, res, next) => {
    try {
      const { id } = req.params;

      const user = await users_history_games.findOne({
        where: {
          id: id,
        },
      });

      const delhistory = await users_history_games.destroy({
        where: {
          id: id,
        },
      });

      if (user.length <= 0) {
        return res.status(404).json({
          status: "failed",
          message: "data was not found",
          data: null,
        });
      }
    } catch (err) {
      next(err);
    }
  },
};
